# Define a method hello(name) that takes a string representing a name and returns the string "Hello, " 
# concatenated with the name.
name = "Ruby"

def hello(name)
    "Hello, " + name
end

hello(name)


# Define a method starts_with_consonant?(s) that takes a string and returns true if it starts with a 
# consonant and false otherwise. (For our purposes, a consonant is any letter other than A, E, I, O, U.) 
# NOTE: be sure it works for both upper and lower case and for nonletters!
str = "An apples"
str2 = "a bird"
str3 = "test"
str4 = ""
str5 = " test"
str6 = "everything i do"

def starts_with_consonant?(s)
    s = s.upcase
    if s.empty? || s == nil
        return false
    end
    return s.start_with?("A","E","I","O","U")?false:true
end

starts_with_consonant?(str6)


# Define a method binary_multiple_of_4?(s) that takes a string and returns true if the string 
# represents a binary number that is a multiple of 4. NOTE: be sure it returns false if the string is 
# not a valid binary number!

s = "011100"
s1 = " 011100 "
s2 = "a011100"
s3 = "0"

def binary_multiple_of_4?(s)
    if s.empty? || s==nil
        return false
    end
    
    if /[^[:digit:]]/.match(s)
        return false
    end
    
    if /[^0-1]/.match(s)
        return false
    end
    
    # num = Integer("0b" + s)
    num = s.to_i(2)
    
    if num % 4 == 0 && num!=0
        return true
    end
    return false
end

binary_multiple_of_4?(s)


