require 'spec_helper'

describe MoviesController do
  describe 'find movies with same director' do
    it 'if found movies with same director, should redirect to same_director page' do
      mock = mock('Movie')
      mock.stub(:director).and_return('mock director')
      
      sameDirectorMocks = [mock('Movie'), mock('Movie')]
      
      Movie.should_receive(:find).with('1').and_return(mock)
      Movie.should_receive(:find_all_by_director).with(mock.director).and_return(sameDirectorMocks)
      get :same_director, {:id => '1'}
      response.should render_template :same_director
    end
    
    it 'if movie does not have a director, should redirect to index page' do
      mock = mock('Movie')
      mock.stub(:director).and_return(nil)
      mock.stub(:title).and_return(nil)
      
      Movie.should_receive(:find).with('1').and_return(mock)
      get :same_director, {:id => '1'}
      response.should redirect_to(movies_path)
    end

    it 'should remove noDirector message when surfing index page' do
      session[:noDirector] = 'test'
      get :index
      session[:noDirector].should == nil
    end
  end
end