# - Create a class Dessert with getters and setters for name and calories. The constructor should accept 
# arguments for name and calories.
# - Define instance methods healthy?, which returns true if and only if a dessert has less than 200 calories, 
# and delicious?, which returns true for all desserts.
# - Create a class JellyBean that inherits from Dessert. The constructor should accept a single argument giving 
# the jelly bean's flavor; a newly-created jelly bean should have 5 calories and its name should be the 
# flavor plus "jelly bean", for example, "strawberry jelly bean".
# - Add a getter and setter for the flavor.
# - Modify delicious? to return false if the flavor is licorice, but true for all other flavors. The behavior 
# of delicious? for non-jelly-bean desserts should be unchanged.

class Dessert
  #attributes
  attr_accessor :name, :calories
  
  #constructors
  def initialize(name, calories)
    @name = starting_name
    @calories = starting_calories
  end
  
  #setters & getters
  def name
    @name
  end
  
  def name=(name)
    @name = name
  end
  
  def calories
    @calories
  end
  
  def calories=(calories) 
    @calories = calories
  end

  #instance methods
  def healthy?
    @calories < 200
  end

  def delicious?
    return true
  end
end

class JellyBean < Dessert
  #attributes
  attr_accessor :flavor

  #constructors
  def initialize(flavor)
    super(flavor + " jelly bean", 5)
    @flavor = flavo
  end

  #setters & getters
  def flavor
    @flavor
  end
  
  def flavor=(flavor)
    @flavor = flavor
  end

  #instance methods
  def delicious?
    return flavor == 'licorice' ? false : true
  end
end
