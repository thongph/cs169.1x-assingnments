# arr = [1, 2, 3, -4, 3]
# arr2 = []
# arr3 = ["a", "3"]
# arr4 = ["a", 5]
# arr5 = [5, "4"]
# arr6 = [1, 2]

arr = [1, 2, 3, -4, 3]
arr1 = []
arr2 = [1, 2]
arr3 = [1]

# Define a method sum which takes an array of integers as an argument and returns the sum of its elements. 
# For an empty array it should return zero.

def sum(arr)
    if arr.empty? || arr == nil
        return 0
    end
    total = 0
    for element in arr
        total += element
    end
    return total
end

sum(arr)

# Define a method max_2_sum which takes an array of integers as an argument and returns the sum of 
# its two largest elements. For an empty array it should return zero. For an array with just one 
# element, it should return that element.

def max_2_sum(arr)
    if arr.empty? || arr == nil
        return 0
    end
    
    if arr.length == 1
        return arr[0]
    end
    
    sum(elements.sort.last(2))
end

max_2_sum(arr)

# Define a method sum_to_n? which takes an array of integers and an additional integer, n, as arguments 
# and returns true if any two distinct elements in the array of integers sum to n. An empty array or 
# single element array should both return false.

def sum_to_n?(arr, n)
    if arr.empty? || arr == nil
        return false
    end
    
    unless n.is_a? Integer
        return false
    end
        
    elements.combination(2).to_a.each do |pair|
      return true if sum(pair) == total
    end
    return false
end

n = 5
sum_to_n?(arr, n)
