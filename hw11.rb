# A palindrome is a word or phrase that reads the same forwards as backwards, ignoring case, punctuation, 
# and nonword characters. (A "nonword character" is defined for our purposes as "a character that Ruby regular 
# expressions would treat as a nonword character".)

# You will write a method palindrome? that returns true if and only if its receiver is a palindrome.

def palindrome? str
  str = str.gsub(/\W/i, '').downcase
  str == str.reverse
end

palindrome?("adam")
palindrome?("redivider")

# Define a function count_words that, given an input string, return a hash whose keys are words in the string 
# and whose values are the number of times each word appears

def count_words str
  str_hash = Hash.new
  str = str.gsub(/\W/i, ' ').downcase.split
  str.each do |word|
    str_hash[word] = str_hash[word].nil? ? 1 : str_hash[word] + 1
  end
  str_hash
end

count_words("to be or not to be")

# An anagram group is a group of words such that any one can be converted into any other just by rearranging 
# the letters. For example, "rats", "tars" and "star" are an anagram group.

def anagram str
  str = str.split(//)
  str = str.permutation(str.count).to_a
  anagram = []
  str.each do |s|
    anagram << s.join
  end
  anagram
  # str = str.downcase.split.group_by {|word| word.chars.sort}.values
end

anagram("star")