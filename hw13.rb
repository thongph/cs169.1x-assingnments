class RockPaperScissors
  class NoSuchStrategyError < StandardError ; end
  def self.winner(players)
    strategy1 = players[0].last
    strategy2 = players[1].last
    raise NoSuchStrategyError, "Strategy must be one of R,P,S" unless (self.is_valid? strategy1 and self.is_valid? strategy2)
    if strategy1!=strategy2 then
      case strategy1
        when "R" then strategy2=="S" ? players[0] : players[1]
        when "P" then strategy2=="R" ? players[0] : players[1]
        when "S" then strategy2=="P" ? players[0] : players[1]
      end
    else
      players[0]
    end
  end

  def self.is_valid?(strategy)
    !!(strategy =~  /^[RPS]{1}$/)
  end

  def self.tournament_winner tournament
    if tournament[0][0].is_a? String
      self.winner(tournament)
    else
      winner = nil
      tournament.each do |t|
        if winner.nil?
          winner = self.tournament_winner(t)
        else
          winner = self.winner [winner, tournament_winner(t)]
        end
      end
      winner
    end
  end
end

t = [["a", "P"], ["b", "S"]]

t2 =
[
  [
    [ ["Armando", "P"], ["Dave", "S"] ],
    [ ["Armando0", "P"], ["Dave0", "S"] ],
    [ ["Richard", "R"],  ["Michael", "P"] ]
  ],
  [
    [ ["Armando1", "P"], ["Dave1", "S"] ]
  ],
  [
    [ ["Armando2", "P"], ["Dave2", "S"] ],
    [ ["Richard8", "R"],  ["Michael8", "S"] ]
  ],
  [
    [ ["Armando3", "P"], ["Dave3", "S"] ],
    [ ["Richard11", "R"],  ["Michael11", "S"] ]
  ]
]

RockPaperScissors.winner(t)

RockPaperScissors.tournament_winner(t)

RockPaperScissors.tournament_winner(t2)