class Class
  def attr_accessor_with_history(attr_name)
    attr_name = attr_name.to_s
    attr_reader attr_name
    attr_reader attr_name+"_history"
    class_eval %Q"
      def #{attr_name}=(value)
        if @#{attr_name+"_history"}.nil?
          @#{attr_name+"_history"} = [nil]
        end
        @#{attr_name} = value
        @#{attr_name+"_history"} << value
      end
      "
    end
  end
end

class MyClass
  attr_accessor_with_history :bar
end

mc = MyClass.new
mc.bar = 5
mc.bar = "test"
mc.bar_history
